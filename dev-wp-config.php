<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Skrypt wp-config.php używa tego pliku podczas instalacji.
 * Nie musisz dokonywać konfiguracji przy pomocy przeglądarki internetowej,
 * możesz też skopiować ten plik, nazwać kopię "wp-config.php"
 * i wpisać wartości ręcznie.
 *
 * Ten plik zawiera konfigurację:
 *
 * * ustawień MySQL-a,
 * * tajnych kluczy,
 * * prefiksu nazw tabel w bazie danych,
 * * ABSPATH.
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'izakasza_portfolio');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'root');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', '');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8mb4');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/_)iYp}i##J;ig7SF/q`yq^$6VulJ79.6!U_v3^C/H5thrPW$E$4d=RM{/,2T kM');
define('SECURE_AUTH_KEY',  'JfTM:W*/jjELBSd0j5/?%?!|v`%h|cHwAom.fXpwNv4T#Tj(*>:j`<33,!|)`B_6');
define('LOGGED_IN_KEY',    'db~s?<6A`KP$o=xhWoxinV[ua~bezH-dXa)?Fm3-3y<(G3^F(}4_q1xu_-jm`;?w');
define('NONCE_KEY',        'Yw/z$i?[R~3.i]Vz|$_(0hFRdWvd;yA$K}O|V*e*V;}P5aO~78jk9j~p,Hx#@4*I');
define('AUTH_SALT',        '6BV~u.;B0.}<xrn$Lc!x$E?@W^W;WBWEk#>qD@*5;2rzHEDrZmhEhwAL.O|(Yw].');
define('SECURE_AUTH_SALT', '.yrtZbDU$4|L+Piel12kYPR}ov)CH,F+^ h}Wyal{~m*FUOVAM1VVl9 kZD51dt[');
define('LOGGED_IN_SALT',   '$twUxV+1R0l1Nfw<Z$X[P_@0ZD6,=xOyrRum%UT&ijy:Q ;y@!r}}qq1m4.#yd4h');
define('NONCE_SALT',       'r!%fmb#M0>]#S}2*TU%dFG~U6p/v|6_&t4ku:=8ny!g_uZG`C XK=qC^`Cw||WTl');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'ik_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie
 * ostrzeżeń podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG podczas pracy nad nimi.
 *
 * Aby uzyskać informacje o innych stałych, które mogą zostać użyte
 * do debugowania, przejdź na stronę Kodeksu WordPressa.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
