var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

var AssetsWebpackPlugin = require('assets-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var LiveReloadPlugin = require('webpack-livereload-plugin');
var SpriteSmithPlugin = require('webpack-spritesmith');
var SvgStore = require('webpack-svgstore-plugin');

var precss = require('precss');
var autoprefixer = require('autoprefixer');
var postcssImport = require('postcss-import');

var bem = require('posthtml-bem')();
var each = require('posthtml-each')();


var PROD = JSON.parse(process.env.PROD_ENV || '0');
var DEV = !PROD;

var settings = {
    destDir: '/public/'
};

settings.root = path.join(__dirname, path.dirname(settings.destDir));

var plugins = [
    new ExtractTextPlugin("[name].css"),
    new AssetsWebpackPlugin(),
    new CleanWebpackPlugin([settings.destDir.split('/').slice(-2)[0]], {
        root: settings.root,
        versbose: true,
        dry: false
    }),
    new SpriteSmithPlugin({
        src: {
            cwd: path.resolve(__dirname, 'app/shared/images/sprite'),
            glob: '*.png'
        },
        target: {
            image:  path.resolve(__dirname, 'app/shared/images/sprite.png'),
            css: path.resolve(__dirname, 'app/shared/sass/sprite.scss')
        },
        apiOptions: {
            cssImageRef: '../images/sprite.png'
        }
    }),
    new SvgStore({
        // svgo options
        svgoOptions: {
            plugins: [
                { removeTitle: true }
            ]
        }
    })
];

if (PROD) {
    plugins.push(new webpack.optimize.UglifyJsPlugin({
        minimize: PROD,
        compress: {
            warnings: false
        },
        mangle: false
    }));
}

if (DEV) {
    plugins.push(new LiveReloadPlugin());
}

plugins.push(new webpack.ProvidePlugin({
    angularScroll: "angularScroll"
}));

module.exports = {
  cache: true,
  context: __dirname,
  entry: './app/portfolio.js',
  output: {
    filename: '[name].js',
    path: path.join(__dirname, "public")
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel', exclude: /node_modules/, query: { presets: ['es2015']}},
      { test: /\.scss$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract("style-loader", "css-loader?" + (PROD ? "minimize" : "-minimize") + "!postcss-loader!sass-loader") },
      { test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader?" + (PROD ? "minimize" : "-minimize") + "") },
      { test: /\.(ttf|otf|eot|woff(2)?)(\?[a-z0-9]+)?$/, loader: 'file-loader?name=fonts/[name].[ext]' },
      { test: /\.(png|jpg|gif)$/, loader: 'file-loader?name=images/[hash].[ext]' },
      { test: /\.html$/, include: /app/, loader: 'ngtemplate?relativeTo=' + path.resolve(__dirname, './app') + '/!html!posthtml'},
      { test: /\.svg$/, loader: "url?limit=8192"},
      { test: /\.font\.(js|json)$/, loader: "style!css!fontgen" },
      { test: /\.modernizrrc$/, loader: "modernizr" }
    ]
  },
  plugins: plugins,
  postcss: function (webpack) {
    return [
        postcssImport({
            addDependencyTo: webpack
        }),
        precss,
        autoprefixer
        ];
  },
  posthtml: function () {
    return {
        defaults: [ bem, each ]
    }
  },
  resolve: {
    alias: {
        modernizr$: path.resolve(__dirname, "./app/.modernizrrc")
    }
  }
};
