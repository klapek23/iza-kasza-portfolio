<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Gardimax
 */

get_header(); ?>

<section class="home-intro scrollme animateme" data-when="exit" data-from="0.7" data-to="0.9" data-opacity="0" data-translatey="-200" data-crop="false">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php the_field('page_title', get_option('page_on_front')); ?> <br>
                    <span><?php the_field('page_subtitle', get_option('page_on_front')); ?> </span>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?php if(get_field('banner_text', get_option('page_on_front'))): ?>
                    <p><?php the_field('banner_text', get_option('page_on_front')); ?></p>
                <?php endif; ?>

                <?php if(get_field('button_label', get_option('page_on_front'))): ?>
                    <a href="<?php the_field('button_link', get_option('page_on_front')); ?>" title="<?php the_field('button_label', get_option('page_on_front')); ?>" class="button brown"><?php the_field('button_label', get_option('page_on_front')); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="content 404" role="main">

    <div id="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section class="basic-content widget scrollme animateme" data-when="enter" data-from="0.5" data-to="0" data-opacity="0.3" data-scale="1.2">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 style="font-size: 62px; font-family: 'opensanslight';"><?php _e('404', 'klapek23_framework'); ?></h2>
                                <h3 style="font-size: 34px"><?php _e("Page doesn't exist", 'klapek23_framework'); ?></h3>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

</section>

<?php
get_footer();
?>