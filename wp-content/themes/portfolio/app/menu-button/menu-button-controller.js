export default class MenuButtonController {
  constructor(menuService){
    this.menuService = menuService;
  }

  onClick($event) {
    this.menuService.toggleMenu();
  }
}

MenuButtonController.$inject = ["menuService"];
