import angular from 'angular';

import MenuButtonController from './menu-button-controller.js';
import MenuService from '../menu/menu-service.js';
import MenuButtonTemplate from './menu-button-template.html';
import MenuButtonStyles from './menu-button-styles.scss';

export default angular.module('menu-button', [])
                .component("menuButtonComponent", {
                  controller: MenuButtonController,
                  templateUrl: MenuButtonTemplate
                });
