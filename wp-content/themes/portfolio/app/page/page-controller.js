export default class PageController{
  constructor(pageService){
    this.pageService = pageService;
  }
}

PageController.$inject = ["pageService"];
