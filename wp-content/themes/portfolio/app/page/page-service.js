export default class PageService {
    constructor($http, $sce) {
        this._$http = $http;
        this._$sce = $sce;
    }

    getPage(id) {
        return this._$http({
            method: 'get',
            url: `/wp-json/wp/v2/pages/${id}`
        }).then( response => {
            return response.data;
        }, (error) => {
            return error;
        });
    };

    getPageBySlug(slug) {
        return this._$http({
            method: 'get',
            url: `/wp-json/wp/v2/pages`,
            params: {
                slug: slug
            }
        }).then( response => {
            let page = response.data[0];
            page.content.rendered = this._$sce.trustAsHtml(page.content.rendered);
            return page;
        }, (error) => {
            return error;
        });
    };
}

PageService.$inject = ['$http', '$sce'];