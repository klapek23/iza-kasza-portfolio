import angular from 'angular';

import PageController from './page-controller.js';
import PageService from './page-service.js';
import PageStyles from './page-styles.scss';

export default angular.module('page', [])
                .service("pageService", PageService)
                .component("pageComponent", {
                  controller: PageController
                });
