export default class PortfolioService {
    constructor($http, $sce) {
        this._$http = $http;
        this._$sce = $sce;
    }

    getItems(offset, limit) {
        return this._$http({
            method: 'get',
            url: '/wp-json/wp/v2/posts',
            params: {
                offset: offset,
                per_page: limit
            }
        }).then((response) => {
            angular.forEach(response.data, (item, key) => {
                response.data[key].title.display = this._$sce.trustAsHtml(item.title.rendered);
                response.data[key].intro = (typeof item.intro === 'string' ? this._$sce.trustAsHtml(item.intro) : item.intro);
                response.data[key].content.rendered = this._$sce.trustAsHtml(item.content.rendered);
            });

            return response.data;
        }, (error) => {
            return error;
        });
    }

    getRandomItems(limit, exclude) {
        return this._$http({
            method: 'get',
            url: '/wp-json/wp/v2/posts',
            params: {
                exclude: exclude,
                per_page: limit
            }
        }).then((response) => {
            angular.forEach(response.data, (item, key) => {
                response.data[key].title.display = this._$sce.trustAsHtml(item.title.rendered);
                response.data[key].intro = (typeof item.intro === 'string' ? this._$sce.trustAsHtml(item.intro) : item.intro);
                response.data[key].content.rendered = this._$sce.trustAsHtml(item.content.rendered);
            });

            return response.data;
        }, (error) => {
            return error;
        });
    }

    getAll() {
        return this._$http({
            method: 'get',
            url: '/wp-json/wp/v2/posts'
        }).then((response) => {
            angular.forEach(response.data, (item, key) => {
                response.data[key].title.display = this._$sce.trustAsHtml(item.title.rendered);
                response.data[key].intro = (typeof item.intro === 'string' ? this._$sce.trustAsHtml(item.intro) : item.intro);
                response.data[key].content.rendered = this._$sce.trustAsHtml(item.content.rendered);
            });

            return response.data;
        }, (error) => {
            return error;
        });
    }

    getBySlug(slug) {
        console.log(slug);
        return this._$http({
            method: 'get',
            url: `/wp-json/wp/v2/posts?slug=${slug}`
        }).then((response) => {
            let item = response.data[0];
            item.title.rendered = this._$sce.trustAsHtml(item.title.rendered);
            item.content.rendered = this._$sce.trustAsHtml(item.content.rendered);
            item.intro = (typeof item.intro === 'string' ? this._$sce.trustAsHtml(item.intro) : item.intro);
            return item;
        }, (error) => {
            return error;
        });
    }
}

PortfolioService.$inject = ['$http', '$sce'];
