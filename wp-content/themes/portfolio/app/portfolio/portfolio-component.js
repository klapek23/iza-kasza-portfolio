import angular from 'angular';

import PortfolioController from './portfolio-controller.js';
import PortfolioService from './portfolio-service.js';
import PortfolioDirective from './portfolio-directive.js';
import PortfolioTemplate from './portfolio-template.html';
import PortfolioStyles from './portfolio-styles.scss';

export default angular.module('portfolio', [])
                .service("portfolioService", PortfolioService)
                .component("portfolioComponent", {
                  controller: PortfolioController,
                  templateUrl: PortfolioTemplate
                })
                .directive("portfolioDirective", ($window) => new PortfolioDirective($window));
