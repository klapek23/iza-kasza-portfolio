export default class PortfolioDirective {
    constructor($window) {
        this.restrict = 'A';
        this.$window = $window;
    }

    link(scope, element, attrs, controller) {
        var lastTop = 0;
        angular.element(this.$window).bind('scroll', ($event) => {
            let elPosition = element[0].getBoundingClientRect(),
                windowScroll = this.$window.scrollY,
                scrollBottom = lastTop < windowScroll;

            if(scope.$ctrl.hasMoreItems && !scope.$ctrl.isLoading && scrollBottom && elPosition.top + (element[0].offsetHeight - this.$window.innerHeight) <= 0) {
                scope.$broadcast('portfolio-scrolled');
            }

            lastTop = windowScroll;
        });
    }
}

PortfolioDirective.$inject = ["$window"];