export default class PortfolioController{
  constructor($scope, $timeout, $window, $state, portfolioService, loaderService){
    this.portfolioService = portfolioService;
    this.loaderService = loaderService;
    this.$scope = $scope;
    this.$timeout = $timeout;
    this.$state = $state;
    this.$window = $window;
    this.loaded = 0;
    this.hasMoreItems = true;

    let windowHeight = this.$window.innerHeight;
    let windowWidth = this.$window.innerWidth;
    switch(true) {
        case windowHeight <= 400 && windowWidth <= 991:
            this.loadOnStart = 4;
            this.loadOnScroll = 4;
            break;
        case windowHeight <= 700 && windowWidth <= 991:
            this.loadOnStart = 4;
            this.loadOnScroll = 4;
            break;
        case windowHeight <= 700 && windowWidth > 991:
            this.loadOnStart = 6;
            this.loadOnScroll = 6;
            break;
        case windowHeight > 700:
            this.loadOnStart = 9;
            this.loadOnScroll = 6;
            break;
        default:
            this.loadOnStart = 6;
            this.loadOnScroll = 6;
    }


    this.loaderService.show();
    this.isLoading = true;
    this.portfolioService.getItems(0, this.loadOnStart).then((items) => {
        this.items = items;
        this.loaded = this.items.length;
        this.loaderService.hide();
        this.isLoading = false;

        if(items.length < this.loadOnStart) {
            this.hasMoreItems = false;
        }
    }, (error) => {
        console.log(error);
    });

    this.$scope.$on('portfolio-scrolled', () => {
        this.getItems(this.loaded, this.loadOnScroll);
    });
  }

  getItems(offset, limit) {
      this.loaderService.show();
      this.isLoading = true;
      this.portfolioService.getItems(offset, limit).then((items) => {
          angular.forEach(items, (item) => {
            item.isAnimated = true;
            this.items.push(item);
          });

          this.loaded = this.items.length;
          this.loaderService.hide();
          this.isLoading = false;

          if(items.length < limit) {
              this.hasMoreItems = false;
          }
      }, (error) => {
         console.log(error);
      });
  }
}

PortfolioController.$inject = ["$scope", "$timeout", "$window", "$state", "portfolioService", "loaderService"];
