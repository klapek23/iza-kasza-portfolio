import angular from 'angular';

import MenuController from './menu-controller.js';
import MenuService from './menu-service.js';
import MenuTemplate from './menu-template.html';
import MenuStyles from './menu-styles.scss';

export default angular.module('menu', [])
                .service("menuService", MenuService)
                .component("menuComponent", {
                  controller: MenuController,
                  templateUrl: MenuTemplate,
                  bindings: {
                      mainMenu: '<',
                      socialMenu: '<'
                  }
                });
