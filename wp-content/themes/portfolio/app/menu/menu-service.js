export default class menuService {
  constructor($http, $sce) {
      this.isOpened = false;
      this._$http = $http;
      this._$sce = $sce;
      this.mainMenu = {};
      this.socialMenu = {};
  }

  toggleMenu() {
      this.isOpened = !this.isOpened;
  }

  getMainMenu() {
      if(Object.keys(this.mainMenu).length > 0) {
          return this.mainMenu;
      } else {
          return this._getMenu('main-menu').then( menu => {
              this.mainMenu = menu;
              return menu;
          }, error => {
              return error;
          });
      }
  }

  getSocialMenu() {
      if(Object.keys(this.socialMenu).length > 0) {
          return this.socialMenu;
      } else {
          return this._getMenu('social-menu').then( menu => {
              angular.forEach(menu.items, (item, key) => {
                  menu.items[key].post_name = this._$sce.trustAsHtml(item.post_name);
              });

              this.socialMenu = menu;
              return menu;
          }, error => {
              return error;
          });
      }
  }

  _getMenu(slug) {
      return this._$http({
          method: 'get',
          url: `/wp-json/menus/v1/menus/${slug}`
      }).then( response => {
          return response.data;
      });
  }
}

menuService.$inject = ['$http', '$sce'];