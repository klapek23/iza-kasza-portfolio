import PageService from '../page/page-service.js';

export default class PageDefaultService extends PageService {
    constructor($http, $sce) {
        super();
        this._$http = $http;
        this._$sce = $sce;
    }

    getAll(url) {
        return this.getPageBySlug(url).then((page) => {
            return page;
        }, (error) => {
            return error;
        });
    }
}

PageDefaultService.$inject = ['$http', '$sce'];