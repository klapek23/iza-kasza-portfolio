import angular from 'angular';

import PageDefaultController from './page-default-controller.js';
import PageDefaultService from './page-default-service.js';
import PageDefaultTemplate from './page-default-template.html';
import PageDefaultStyle from './page-default-style.scss';

export default angular.module('page-default', [])
                .service("pageDefaultService", PageDefaultService)
                .component("pageDefaultComponent", {
                  controller: PageDefaultController,
                  templateUrl: PageDefaultTemplate,
                  bindings: {
                      url: '@'
                  }
                });
