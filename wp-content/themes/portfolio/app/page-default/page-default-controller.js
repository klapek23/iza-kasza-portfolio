import PageController from '../page/page-controller.js';

export default class PageDefaultController extends PageController{
  constructor($q, pageDefaultService, loaderService){
    super();
    this.$q = $q;
    this.pageDefaultService = pageDefaultService;
    this.loaderService = loaderService;

    this.loaderService.show();
      this.pageDefaultService.getAll(this.url).then((response) => {
          this.page = response;
          this.loaderService.hide();
      }, (error) => {
        console.log(error);
      });
  }
}

PageDefaultController.$inject = ["$q", "pageDefaultService", "loaderService"];
