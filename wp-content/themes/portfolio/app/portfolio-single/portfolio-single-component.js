import angular from 'angular';

import PortfolioSingleController from './portfolio-single-controller.js';
import PortfolioSingleTemplate from './portfolio-single-template.html';
import PortfolioSingleStyles from './portfolio-single-styles.scss';

export default angular.module('portfolio-single', [])
                .component("portfolioSingleComponent", {
                  controller: PortfolioSingleController,
                  templateUrl: PortfolioSingleTemplate
                });
