export default class PortfolioSingleController{
  constructor($timeout, $state, portfolioService, loaderService){
    this.portfolioService = portfolioService;
    this.loaderService = loaderService;
    this.$timeout = $timeout;
    this.$state = $state;
    this.slug = this.$state.params.item;

    this.loaderService.show();
    this.portfolioService.getBySlug(this.slug).then((item) => {
        this.item = item;

        this.portfolioService.getRandomItems(6, this.item.id).then((items) => {
            angular.forEach(items, (item, key) => {
                item.animated = false;
                item.visible = true;
            });

            this.seeAlsoItems = items;
            this.loaderService.hide();
        }, (err) => {
            console.log(err);
        });
    }, (error) => {
        console.log(error);
    });
  }
}

PortfolioSingleController.$inject = ["$timeout", "$state", "portfolioService", "loaderService"];
