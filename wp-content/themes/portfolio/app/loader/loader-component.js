import angular from 'angular';

import LoaderController from './loader-controller.js';
import LoaderService from './loader-service.js';
import LoaderTemplate from './loader-template.html';
import LoaderStyles from './loader-styles.scss';

export default angular.module('loader', [])
                .service("loaderService", LoaderService)
                .component("loaderComponent", {
                  controller: LoaderController,
                  templateUrl: LoaderTemplate
                });
