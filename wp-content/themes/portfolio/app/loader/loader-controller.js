export default class LoaderController{
  constructor(loaderService){
    this.loaderService = loaderService;
  }
}

LoaderController.$inject = ["loaderService"];
