import angular from 'angular';
import ngNewRouter from 'angular-new-router/index';

import angularAnimate from 'angular-animate';
import uirouter from 'angular-ui-router';

var angularScroll = require('angular-scroll');

//import bootstrap css components
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

require("./shared/fonts/icon.font.js");

//import main scss file
import './shared/sass/app.scss';

//modernizr fixes
require("!modernizr!./.modernizrrc");
Modernizr.addTest('flexboxtweener', Modernizr.testAllProps('flexAlign', 'end', true));

import FixedHeader from './shared/fixed-header/fixed-header.directive.js';
import BackToTop from './shared/back-to-top/back-to-top.directive.js';
import MenuComponent from './menu/menu-component.js';
import LoaderComponent from './loader/loader-component.js';
import MenuButtonComponent from './menu-button/menu-button-component.js';
import PortfolioComponent from './portfolio/portfolio-component.js';
import PortfolioSingleComponent from './portfolio-single/portfolio-single-component.js';
import PageComponent from './page/page-component.js';
import PageDefaultComponent from './page-default/page-default-component.js';
import PageContactComponent from './page-contact/page-contact-component.js';

export default angular.module("app", [angularAnimate, uirouter, ngNewRouter, MenuComponent.name, MenuButtonComponent.name, PortfolioComponent.name, PortfolioSingleComponent.name,  LoaderComponent.name, PageComponent.name, PageDefaultComponent.name, PageContactComponent.name, angularScroll])
    .directive('fixedHeader', FixedHeader)
    .directive('backToTop', ($window, $document) => new BackToTop($window, $document))
    .config(($stateProvider, $locationProvider, $urlRouterProvider) => {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('portfolio', {
                abstract: true,
                template: `<ui-view></ui-view>`
            })
            .state('portfolio.list', {
                url: '/',
                template: `<portfolio-component></portfolio-component>`
            })
            .state('portfolio.item', {
                url: '/portfolio/:item/',
                template: `<portfolio-single-component></portfolio-single-component>`
            })
            .state('about-me', {
                url: '/about-me/',
                template: `<page-default-component url='about-me'></page-default-component>`
            })
            .state('contact', {
                url: '/contact/',
                template: `<page-contact-component></page-contact-component>`
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    })
    .run(($rootScope, menuService) => {
        $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
            menuService.isOpened = false;
        });
    })