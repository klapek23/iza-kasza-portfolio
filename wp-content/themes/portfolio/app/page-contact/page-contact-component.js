import angular from 'angular';

import PageContactController from './page-contact-controller.js';
import PageContactService from './page-contact-service.js';
import PageContactTemplate from './page-contact-template.html';
import PageContactStyles from './page-contact-styles.scss';

export default angular.module('page', [])
                .service("pageContactService", PageContactService)
                .component("pageContactComponent", {
                  controller: PageContactController,
                  templateUrl: PageContactTemplate
                });
