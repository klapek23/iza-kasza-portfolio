import PageController from '../page/page-controller.js';

export default class PageContactController extends PageController{
  constructor($q, pageContactService, loaderService, menuService){
    super();
    this.$q = $q;
    this.pageContactService = pageContactService;
    this.loaderService = loaderService;
    this.menuService = menuService;

    this.loaderService.show();

      this.$q.all([
          this.pageContactService.getAll(),
          this.menuService.getSocialMenu()
      ]).then((data) => {
          this.page = data[0];
          this.socialMenu = data[1];
          this.loaderService.hide();
      }, (err) => {
          console.log(err);
      });
  }
}

PageContactController.$inject = ["$q", "pageContactService", "loaderService", "menuService"];
