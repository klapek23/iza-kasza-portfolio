import PageService from '../page/page-service.js';

export default class PageContactService extends PageService {
    constructor($http, $sce) {
        super();
        this._$http = $http;
        this._$sce = $sce;
    }

    getAll() {
        return this.getPageBySlug('contact').then((page) => {
            return page;
        }, (error) => {
            return error;
        });
    }
}

PageContactService.$inject = ['$http', '$sce'];