import BackToTopStyle from './back-to-top.scss';

export default function BackToTop($window, $document) {
    this.restrict = 'A';
    this.link = function(scope, elem) {
        elem.on('click', () => {
            $document.scrollTop(0, 800);
        });
    };
}

BackToTop.$inject = ["$window", "$document"];