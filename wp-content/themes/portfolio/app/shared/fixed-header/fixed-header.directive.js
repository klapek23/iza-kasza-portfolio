import FixedHeaderStyle from './fixed-header.scss';

export default function FixedHeader($window) {
    return {
        restrict: 'A',
        link: (scope, elem) => {
            angular.element($window).bind('scroll', () => {
                let body = angular.element(document.querySelector('body'));

                if($window.pageYOffset > 10) {
                    body.addClass('scrolled');
                    elem.addClass('fixed');
                } else {
                    body.removeClass('scrolled');
                    elem.removeClass('fixed');
                }
            });
        }
    }
}

FixedHeader.$inject = ['$window'];