<!DOCTYPE html>
<!--[if lt IE 7]><html <?php language_attributes(); ?>  class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html <?php language_attributes(); ?>  class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html <?php language_attributes(); ?>  class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html <?php language_attributes(); ?>  class="no-js"> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo get_bloginfo('name'); ?> | <?php the_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index,follow">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/img/favicon.ico?v1.1'; ?>">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-57x57.png';?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-60x60.png';?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-72x72.png';?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-76x76.png';?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-114x114.png';?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-120x120.png';?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-144x144.png';?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-152x152.png';?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() . '/favicon/apple-icon-180x180.png';?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri() . '/favicon/android-icon-192x192.png';?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() . '/favicon/favicon-32x32.png';?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri() . '/favicon/favicon-96x96.png';?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() . '/favicon/favicon-16x16.png';?>">
    <link rel="manifest" href="<?php echo get_template_directory_uri() . '/favicon/manifest.json';?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri() . '/favicon/ms-icon-144x144.png';?>">
    <meta name="theme-color" content="#ffffff">

    <?php
    wp_head();

    $mainMenu = wp_get_nav_menu_items('main-menu');
    $socialMenu = wp_get_nav_menu_items('social-menu');

    $gaCode = get_option('ga-code');

    if($gaCode) {
        echo stripslashes($gaCode);
    }
    ?>
</head>

<body <?php if(is_home()) { echo 'class="home"'; }; ?> ng-app="app">

<div class="frame">
    <div></div>
    <div></div>
    <div></div>
</div>

<header id="main-header" fixed-header>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-10">
                <a ui-sref="portfolio.list" class="logo" title="Iza Kasza - portfolio">
                    <h1>IZA KASZA
                    <span>Graphic Designer<i>&</i>Illustrator</span></h1>
                </a>
            </div>
            <div class="col-md-offset-7 col-md-1 col-sm-7 col-sm-offset-0 col-xs-2">
                <menu-button-component></menu-button-component>
            </div>
        </div>


        <menu-component
            social-menu='<?php echo json_encode($socialMenu);?>'
            main-menu='<?php echo json_encode($mainMenu); ?>'
        ></menu-component>
    </div>
</header>
