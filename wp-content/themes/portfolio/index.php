<?php
get_header(); ?>

    <div class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="app-viewport">
                        <loader-component></loader-component>
                        <ui-view></ui-view>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>