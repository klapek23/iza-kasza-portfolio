<?php
/**
 * OperatorSystems functions and definitions
 *
 */


/* ---------- require classes ------ */
//setup class
require_once('libs/SetupClass.php');

//remove admin pages class
require_once('libs/RemoveAdminPagesClass.php');

//register widgetized area class
require_once('libs/RegisterWidgetizedAreaClass.php');

//theme settings page class
require_once('libs/ThemeSettingsPageClass.php');

//mime type class
require_once('libs/MimeTypeClass.php');

//extend WP REST API
require_once('libs/ExtendWpRestApi.php');


/* ---------- create objects and add actions ----- */
//operator setup
$setup = new SetupClass();
add_action('init', array($setup, 'do_output_buffer'));
add_action('wp_enqueue_scripts', array($setup, 'operator_scripts'));
add_filter( 'widget_title', array($setup, 'html_widget_title'));
$setup->loadTextdomain();
$setup->addThumbnails();
$setup->registerMenus();
$setup->addHTML5();
$setup->widgetDoShortcodes();
$setup->addImagesSizes();
$setup->disableAdminBar();


//register widgetized area for WP
$registerWidgets = new RegisterWidgetizedAreaClass();
add_action( 'widgets_init', array($registerWidgets, 'widgets_init'));


//remove admin pages action
$removeAdminPages = new RemoveAdminPagesClass();
add_action("admin_menu", array($removeAdminPages, "removePostsPage"));


// add action to display theme options page
$addThemeSettignsPage = new ThemeSettingsPageClass();
add_action("admin_menu", array($addThemeSettignsPage, "addPageToMenu"));

//add cross domain cookies
/*$crossDomainCookies = new crossDomainCookiesClass();
add_action("init", array($crossDomainCookies, "init"));*/

// add action to display theme options page
$mimeTypes = new MimeTypeClass();
add_filter('upload_mimes', array($mimeTypes, 'custom_upload_mimes'));

$extendWpRestApi = new ExtendWpRestApi();
add_action( 'rest_api_init', array($extendWpRestApi, 'addItemSlug'));
add_action( 'rest_api_init', array($extendWpRestApi, 'addItemIntro'));
add_action( 'rest_api_init', array($extendWpRestApi, 'addItemThumb'));
add_action( 'rest_api_init', array($extendWpRestApi, 'addItemTags'));

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
    $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
    return $html;
}

?>