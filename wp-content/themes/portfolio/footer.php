<footer id="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-logo">
                    <img src="<?php echo get_option('footer-logo'); ?>" alt="Iza Kasza signin">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="info">
                    <span class="copyright small-text">&copy; izakasza 2016 All rights reserved</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <nav class="social-nav" id="social-nav">
                    <ul>
                        <?php
                        $socialMenu = wp_get_nav_menu_items('social-menu');
                        foreach($socialMenu as $key => $item) { ?>
                            <li class="<?php echo $item->post_name; ?>">
                                <a href="<?php echo $item->url; ?>" title="<?php echo $item->post_title; ?>"
                                   target="_blank" class="<?php echo $item->classes[0]; ?>">
                                    <i class="icon icon-<?php echo $item->classes[0] . '-transparent'; ?>"></i>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="go-top">
                    <span class="small-text go-top-link" back-to-top>Back to top <i class="icon icon-empty-triangle-left"></i>
                </span>
                </div>
            </div>
        </div>
    </div>
    <?php wp_footer(); ?>
</footer>

</div>

</body>
</html>