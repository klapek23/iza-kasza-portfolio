<?php

class SetupClass {

    //make redirections available
    public function do_output_buffer() {
        ob_start();
    }

    //load plugin textdomain (available for translations)
    public function loadTextdomain() {
        load_theme_textdomain( 'ikframework', get_template_directory() . '/languages' );
    }

    //add thumbnails support
    public function addThumbnails() {
        add_theme_support( 'post-thumbnails' );
    }

    //register menus
    public function registerMenus() {
        register_nav_menus( array(
            'primary' => __( 'Main menu', 'main-menu' ),
        ));
        register_nav_menus( array(
            'lang' => __( 'Social menu', 'social-menu' ),
        ));
    }

    //add post formats support
    /*public function addPostFormat() {
        add_theme_support( 'post-formats', array('aside', 'image', 'video', 'quote', 'link' ));
    }*/

    //add HTML5 support for search form
    public function addHTML5() {
        add_theme_support( 'html5', array( 'search-form') );
    }
        
    //add support for do shortcodes in text widget
    public function widgetDoShortcodes() {
        add_filter('widget_text', 'do_shortcode');
    }

    public function addImagesSizes() {
        add_image_size( 'post-thumbnail', '150', '100', true);
        add_image_size( 'portfolio-thumbnail', 360, 300, true);
    }

    public function disableAdminBar() {
        show_admin_bar(false);
    }

    //enqueue additional scripts and styles
    function operator_scripts() {
        wp_deregister_script( 'jquery' );

        wp_register_style('appstyles', get_template_directory_uri() . (getenv('APPLICATION_ENV') == 'dev' ? '/css/build/dev/style.css' : '/public/main.css?v2') . '"' );
        wp_register_script('appscripts', get_template_directory_uri() . (getenv('APPLICATION_ENV') == 'dev' ? '/js/build/dev/scripts.js' : '/public/main.js?v2') . '"');

        wp_enqueue_style('appstyles');
        wp_enqueue_script('appscripts');
    }

    //render widgets titles as html
    public function html_widget_title( $title ) {
        $title = str_replace( '[', '<', $title );
        $title = str_replace( '[/', '</', $title );

        $title = str_replace( 's]', 'strong>', $title );
        $title = str_replace( 'e]', 'em>', $title );
        $title = str_replace('br]', 'br>', $title);

        return $title;
    }

}

?>