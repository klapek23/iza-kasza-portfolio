<?php
/**
 * Created by PhpStorm.
 * User: klapek
 * Date: 2016-07-10
 * Time: 23:24
 */

class ExtendWpRestApi {

    public function addItemSlug() {
        register_rest_field( 'post',
            'slug',
            array(
                'get_callback'    => array($this, 'getItemSlug'),
                'update_callback' => null,
                'schema'          => null,
            )
        );
    }

    public function getItemSlug( $object, $field_name, $request ) {
        $post = get_post( $object['id'] );
        $slug = $post->post_name;

        return $slug;
    }



    public function addItemIntro() {
        register_rest_field( 'post',
            'intro',
            array(
                'get_callback'    => array($this, 'getItemIntro'),
                'update_callback' => null,
                'schema'          => null,
            )
        );
    }

    public function getItemIntro( $object, $field_name, $request ) {
        $intro = get_field('intro', $object['id']);
        return $intro;
    }



    public function addItemThumb() {
        register_rest_field( 'post',
            'thumb',
            array(
                'get_callback'    => array($this, 'getItemThumb'),
                'update_callback' => null,
                'schema'          => null,
            )
        );
    }

    public function getItemThumb( $object, $field_name, $request ) {
        return get_the_post_thumbnail_url($object['id'], 'portfolio-thumbnail');
    }



    public function addItemTags() {
        register_rest_field( 'post',
            'tags',
            array(
                'get_callback'    => array($this, 'getItemTags'),
                'update_callback' => null,
                'schema'          => null,
            )
        );
    }

    public function getItemTags( $object, $field_name, $request ) {
        return wp_get_post_tags($object['id']);
    }
}

