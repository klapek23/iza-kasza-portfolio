<?php
    /*require( dirname(__FILE__) . '/../../../../wp-load.php');

	//set config
	$requiredFields = array('name', 'email', 'message');
	$emailFields = array('email');
	$requiredMessage = $_POST['required_message'];
	$emailMessage = $_POST['email_message'];
	$errors = null;
    $smtp_settings = array(
        'host' => get_option('mailserver_url'),
        'username' => get_option('mailserver_login'),
        'password' => get_option('mailserver_pass'),
        'port' => get_option('mailserver_port'),
        'from' => get_option('blogname')
    );*/

    $requiredFields = array('full_name', 'email', 'message');
    $emailFields = array('email');
    $errors = null;
	
	//get data
    $formData = $_POST;

    $error_messages = array(
        'required' => get_field('form_required_message', $formData['pageID']),
        'email' => get_field('form_email_message', $formData['pageID'])
    );
	
	//get errors
	foreach($formData as $name => $value):
		//check required
		if(in_array($name, $requiredFields)):
			if($value == NULL || $value == ''):
				$errors[$name][] = $error_messages['required'];
			endif;
		endif;
		//check emails
		if(in_array($name, $emailFields)):
			if(!filter_var($value, FILTER_VALIDATE_EMAIL)):
				$errors[$name][] = $error_messages['email'];
			endif;
		endif;
	endforeach;
	
	//set form status
	if($errors != null):
		$status = false;
	else:
		require 'php-mailer/class.phpmailer.php';

		$mail = new PHPMailer;

		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
		$mail->CharSet = 'UTF-8';

		$body    = '
			<h3>Wiadomośc wysłana przez formularz kontaktowy</h3>
			<table cellpaddin="0" cellspacing="0">
				<tr>
					<td>Imię</td>
					<td>'.$formData["full_name"].'</td>
				</tr>
				<tr>
					<td>Email</td>
					<td>'.$formData["email"].'</td>
				</tr>
				<tr>
					<td>Message</td>
					<td>'.$formData["message"].'</td>
				</tr>
			</table>
		';
		
		$mail->SetFrom(get_option('mailserver_login'), get_option('blogname'));
		$mail->Subject    = "Calipso - kontakt";
		
		$mail->MsgHTML($body);		
		$mail->AddAddress(get_option('mailserver_login'), "Administrator");
		
		if(!$mail->send()) {
		    $status = false;
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
            unset($_POST);
		} else {
		    $status = true;
		}
	endif;
	
	$return = array(
		'status' => $status,
		'errors' => $errors
	);
?>